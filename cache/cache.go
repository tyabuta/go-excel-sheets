package cache

import (
	"encoding/json"
	"fmt"
	"gitlab.com/tyabuta/go-excel-sheets/excel"
	"gitlab.com/tyabuta/go-excel-sheets/util"
	"io/ioutil"
	"os"
	"sync"
)

type Cache struct {
	path    string
	mapping internalMap
}

type internalMap map[string]*excel.FileInfo

var mutex = sync.Mutex{}

func New(path string) *Cache {
	return &Cache{path: path, mapping: make(internalMap)}
}

func (c *Cache) Get(name string) (*excel.FileInfo, bool) {
	mutex.Lock()

	ret, ok := c.mapping[name]

	mutex.Unlock()
	return ret, ok
}

func (c *Cache) Set(name string, info *excel.FileInfo) {
	mutex.Lock()

	c.mapping[name] = info

	mutex.Unlock()
}

func (c *Cache) Len() int {
	return len(c.mapping)
}

func (c *Cache) Read() error {

	// Skip reading if the cache file not exists.
	if !util.FileExists(c.path) {
		return nil
	}

	bytes, err := ioutil.ReadFile(c.path)
	if err != nil {
		return err
	}

	err = json.Unmarshal(bytes, &c.mapping)
	if err != nil {
		return fmt.Errorf("failed to parse cache file:%s %v", c.path, err)
	}

	return nil
}

func (c *Cache) Save() error {

	// 存在しなくなったものはキャッシュから削除する
	for k, v := range c.mapping {
		if !v.Exists {
			delete(c.mapping, k)
		}
	}

	jsonBytes, err := json.Marshal(&c.mapping)
	if err != nil {
		return fmt.Errorf("failed to save the cache. %v", err)
	}

	err = ioutil.WriteFile(c.path, jsonBytes, os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to write cache file:%s %v", c.path, err)
	}

	return nil
}
