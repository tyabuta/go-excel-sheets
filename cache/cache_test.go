package cache

import (
	ast "github.com/stretchr/testify/assert"
	"gitlab.com/tyabuta/go-excel-sheets/excel"
	"io/ioutil"
	"log"
	"os"
	"testing"
)

var tempDirName string

func TestSave(t *testing.T) {
	assert := ast.New(t)

	caching := New(tempDirName + "/a.cache")
	err := caching.Read()
	assert.Nil(err, "Don't occur an error when no files.")

	err = caching.Save()
	assert.Nil(err)

	bytes, err := ioutil.ReadFile(caching.path)
	assert.Nil(err)

	assert.Equal(string(bytes), "{}")
}

func TestSaveWithContains(t *testing.T) {
	assert := ast.New(t)

	caching := New(tempDirName + "/b.cache")
	err := caching.Read()
	assert.Nil(err, "Don't occur an error when no files.")

	caching.Set("a", &excel.FileInfo{Exists: true})
	caching.Set("b", &excel.FileInfo{Exists: true})
	caching.Set("c", &excel.FileInfo{Exists: true})

	assert.Equal(3, caching.Len())

	err = caching.Save()
	assert.Nil(err)

	bytes, err := ioutil.ReadFile(caching.path)
	assert.Nil(err)

	assert.NotEqual(string(bytes), "{}")
}

func TestMain(m *testing.M) {
	var err error

	tempDirName, err = ioutil.TempDir("", "")
	if err != nil {
		log.Fatalf("failed to create tmp directory. %v", err)
	}

	defer func() {
		err := os.RemoveAll(tempDirName)
		if err != nil {
			log.Printf("failed to remove tmp directory. %v", err)
		}
	}()

	ret := m.Run()
	os.Exit(ret)
}
