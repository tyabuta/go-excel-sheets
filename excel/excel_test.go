package excel

import (
	"testing"
)

func TestNotContainsWorkbook(t *testing.T) {
	expect := `xl/workbook.xml not contains in testdata/not_contains_workbook.xlsx`

	_, err := GetSheetNames("testdata/not_contains_workbook.xlsx")
	if err == nil {
		t.Errorf("expect:%s actual:nil\n", expect)
		return
	}

	if expect != err.Error() {
		t.Errorf("\nexpect: %s\nactual: %s\n", expect, err.Error())
		return
	}
}
