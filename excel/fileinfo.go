package excel

import (
	"os"
	"time"
)

type FileInfo struct {
	Name         string    `json:"name"`
	RelativePath string    `json:"relativePath"`
	AbsolutePath string    `json:"absolutePath"`
	ModTime      time.Time `json:"modTime"`
	Sheets       []string  `json:"sheets"`
	Exists       bool      `json:"-"`
}

func (e *FileInfo) Path(isFull bool) string {
	if isFull {
		return e.AbsolutePath
	}
	return e.RelativePath
}

func (e *FileInfo) IsLatest(f os.FileInfo) bool {
	return e.ModTime.Equal(f.ModTime())
}
