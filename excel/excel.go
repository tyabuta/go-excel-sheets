package excel

import (
	"archive/zip"
	"bytes"
	"encoding/xml"
	"fmt"
	"gitlab.com/tyabuta/go-excel-sheets/util"
	"io"
)

type xmlSheet struct {
	Name string `xml:"name,attr"`
}
type xmlSheets struct {
	Sheets []xmlSheet `xml:"sheets>sheet"`
}

// unzip excel file and get the sheet names
func GetSheetNames(excelPath string) ([]string, error) {
	r, err := zip.OpenReader(excelPath)
	if err != nil {
		return nil, err
	}
	defer util.SafeClose(r)

	const workbookName = "xl/workbook.xml"
	for _, f := range r.File {
		if workbookName != f.Name {
			continue
		}

		sheetNames, err := readSheetNames(f)
		if nil != err {
			return nil, err
		}
		return sheetNames, nil
	}
	return nil, fmt.Errorf("%s not contains in %s", workbookName, excelPath)
}

func readSheetNames(f *zip.File) ([]string, error) {
	rc, err := f.Open()
	if err != nil {
		return nil, err
	}
	defer util.SafeClose(rc)

	to := new(bytes.Buffer)
	_, err = io.Copy(to, rc)
	if err != nil {
		return nil, err
	}

	v := xmlSheets{}
	if err := xml.Unmarshal(to.Bytes(), &v); nil != err {
		return nil, err
	}

	sheetNames := make([]string, len(v.Sheets))
	for i, s := range v.Sheets {
		sheetNames[i] = s.Name
	}

	return sheetNames, nil
}
