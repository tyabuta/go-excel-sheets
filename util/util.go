package util

import (
	"fmt"
	"io"
	"os"
	"strings"
)

func SafeClose(c io.Closer) {
	err := c.Close()
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, err)
	}
}

// get extension of the file name.
func FileExtension(path string) string {
	pos := strings.LastIndex(path, ".")
	if -1 == pos {
		return ""
	}
	ext := path[pos:]
	return strings.ToLower(ext)
}

func DirectoryExists(path string) bool {
	fileInfo, err := os.Stat(path)
	return err == nil && fileInfo.IsDir()
}


func FileExists(path string) bool {
	fileInfo, err := os.Stat(path)
	return err == nil && !fileInfo.IsDir()
}
