package main

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestErrorFormat(t *testing.T) {
	invalidJson := `{aaaa}`
	var v interface{}

	err := json.Unmarshal([]byte(invalidJson), v)
	err = fmt.Errorf("msg: %v", err)
	t.Log(err)
}
