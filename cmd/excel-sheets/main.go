package main

import (
	"errors"
	"fmt"
	"github.com/urfave/cli"
	"gitlab.com/tyabuta/go-excel-sheets/cache"
	"gitlab.com/tyabuta/go-excel-sheets/excel"
	"gitlab.com/tyabuta/go-excel-sheets/util"

	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"
)

var (
	version   = "xxxxxxxxx"
	revision  = "xxxxxxxxx"
	builtAt   = "xxxxxxxxx"
	goVersion = "xxxxxxxxx"
)
var App *cli.App

var DelimOption string
var CacheFilenameOption string
var FullPathFlag bool
var CachedFlag bool

var HelpTemplate = `NAME:
   {{.Name}}{{if .Usage}} - {{.Usage}}{{end}}

USAGE:
   {{if .UsageText}}{{.UsageText}}{{else}}{{.HelpName}} {{if .VisibleFlags}}[options]{{end}} {{if .ArgsUsage}}{{.ArgsUsage}}{{else}}[arguments...]{{end}}{{end}}{{if .Version}}{{if not .HideVersion}}

VERSION:
   {{.Version}}{{end}}{{end}}{{if .Description}}

DESCRIPTION:
   {{.Description}}{{end}}{{if len .Authors}}

AUTHOR{{with $length := len .Authors}}{{if ne 1 $length}}S{{end}}{{end}}:
   {{range $index, $author := .Authors}}{{if $index}}
   {{end}}{{$author}}{{end}}{{end}}{{if .VisibleCommands}}

OPTIONS:
   {{range $index, $option := .VisibleFlags}}{{if $index}}
   {{end}}{{$option}}{{end}}{{end}}

`

// fileName => CacheData
var caching *cache.Cache

func init() {
	cli.VersionPrinter = func(c *cli.Context) {
		fmt.Printf("version %s (rev:%s)\n", c.App.Version, revision)
		fmt.Printf("built by %s at %s\n", goVersion, builtAt)
	}

	App = cli.NewApp()
	App.CustomAppHelpTemplate = HelpTemplate
	App.Name = "excel-sheets"
	App.Usage = "list of excel sheets."
	App.Version = version
	App.Author = "tyabuta"
	App.Email = "gmforip@gmail.com"
	App.ArgsUsage = "<Dir>"
	App.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "delim, d",
			Value:       "\t",
			Destination: &DelimOption,
			Usage:       "Use delim as the field delimiter character instead of the tab character."},
		cli.BoolFlag{
			Name:        "fullname, F",
			Destination: &FullPathFlag,
			Usage:       "Show full pathname."},
		cli.BoolFlag{
			Name:        "cache, c",
			Destination: &CachedFlag,
			Usage:       "Cached list of files."},
		cli.StringFlag{
			Name:        "cached-file, f",
			Value:       ".excel-sheets.cache",
			Destination: &CacheFilenameOption,
			Usage:       "Cached filename."},
	}
}

func getOrCreate(dirName string, fileInfo os.FileInfo) *excel.FileInfo {
	var excelInfo *excel.FileInfo

	excelInfo, ok := caching.Get(fileInfo.Name())
	if CachedFlag && ok && excelInfo.IsLatest(fileInfo) {
		excelInfo.RelativePath = filepath.Join(dirName, fileInfo.Name())
		excelInfo.AbsolutePath, _ = filepath.Abs(excelInfo.RelativePath)
		excelInfo.Exists = true
		return excelInfo
	}

	excelInfo = &excel.FileInfo{}
	excelInfo.Name = fileInfo.Name()
	excelInfo.ModTime = fileInfo.ModTime()
	excelInfo.RelativePath = filepath.Join(dirName, fileInfo.Name())
	excelInfo.AbsolutePath, _ = filepath.Abs(excelInfo.RelativePath)
	excelInfo.Exists = true

	excelInfo.Sheets, _ = excel.GetSheetNames(excelInfo.RelativePath)
	caching.Set(excelInfo.Name, excelInfo)

	return excelInfo
}

func doFind(dirName string) {
	files, err := ioutil.ReadDir(dirName)
	if err != nil {
		panic(err)
	}

	wg := &sync.WaitGroup{}
	out := make(chan *excel.FileInfo, 100)
	done := make(chan struct{})

	for _, fileInfo := range files {
		if fileInfo.IsDir() {
			continue
		}
		ext := util.FileExtension(fileInfo.Name())
		if ".xlsx" != ext {
			continue
		}
		if "~" == fileInfo.Name()[:1] {
			continue
		}

		wg.Add(1)
		go func(f os.FileInfo) {
			defer wg.Done()
			out <- getOrCreate(dirName, f)
		}(fileInfo)

	}

	go func() {
		defer close(done)
		for excelInfo := range out {
			outputSheets(excelInfo)
		}
	}()

	wg.Wait()
	close(out)
	<-done
}

func outputSheets(excelInfo *excel.FileInfo) {
	for _, sheetName := range excelInfo.Sheets {
		fmt.Printf("%s%s%s\n", excelInfo.Path(FullPathFlag), DelimOption, sheetName)
	}
}

func main() {
	App.Action = func(c *cli.Context) error {
		if 1 > c.NArg() {
			cli.ShowAppHelpAndExit(c, 1)
		}

		dirPath := c.Args()[0]
		if !util.DirectoryExists(dirPath) {
			return errors.New("No such directory:" + dirPath)
		}

		caching = cache.New(CacheFilenameOption)
		if CachedFlag {
			err := caching.Read()
			if err != nil {
				return err
			}
		}

		doFind(dirPath)

		if CachedFlag {
			err := caching.Save()
			if err != nil {
				return err
			}
		}
		return nil
	}

	err := App.Run(os.Args)
	if err != nil {
		log.Fatalln(err)
	}
}
