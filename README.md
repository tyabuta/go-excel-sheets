excel-sheets
============

## Go get

```bash
go get -v gitlab.com/tyabuta/go-excel-sheets
```


## Install commands

```bash
go get -v gitlab.com/tyabuta/go-excel-sheets/cmd/...
```


## Usage

### excel-sheets --help

```text
NAME:
   excel-sheets - list of excel sheets.

USAGE:
   main [options] <Dir>

VERSION:
   0.1.0

AUTHOR:
   tyabuta <gmforip@gmail.com>

OPTIONS:
   --delim value, -d value        Use delim as the field delimiter character instead of the tab character. (default: "\t")
   --fullname, -F                 Show full pathname.
   --cache, -c                    Cached list of files.
   --cached-file value, -f value  Cached filename. (default: ".excel-sheets.cache")
   --help, -h                     show help
   --version, -v                  print the version
```


## Output example

```
sample1.xlsx    sheet1
sample1.xlsx    sheet2
sample1.xlsx    sheet3
sample2.xlsx    sheet1
sample2.xlsx    sheet2
sample2.xlsx    sheet3
sample3.xlsx    sheet1
sample3.xlsx    sheet2
sample3.xlsx    sheet3
```


