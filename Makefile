binName=excel-sheets
cmdPackage=./cmd/excel-sheets

VERSION=0.3.0
REV=$(shell git rev-parse --verify HEAD)
BUILT_AT=$(shell date '+%Y-%m-%d %H:%M:%S %Z')
GO_VERSION=$(shell go version)

LDFLAGS=-ldflags "-X main.version=$(VERSION) -X main.revision=$(REV) -X \"main.builtAt=$(BUILT_AT)\" -X \"main.goVersion=$(GO_VERSION)\""


test:
	go test ./...

run-sample:
	go run $(cmdPackage) ./sample_xlsx

run-sample-cache:
	go run $(cmdPackage) -c ./sample_xlsx

run-help:
	go run $(cmdPackage) --help


dep:
	go get -v -u github.com/urfave/cli
	go get -v -u gitlab.com/tyabuta/go-util

show-cache:
	jq . .excel-sheets.cache

build:
	go build $(LDFLAGS) -o bin/$(binName) $(cmdPackage)

build-mac:
	GOOS=darwin  GOARCH=amd64 go build $(LDFLAGS) -o bin/darwin64/$(binName)      $(cmdPackage)
build-linux64:
	GOOS=linux   GOARCH=amd64 go build $(LDFLAGS) -o bin/linux64/$(binName)       $(cmdPackage)
build-win64:
	GOOS=windows GOARCH=amd64 go build $(LDFLAGS) -o bin/windows64/$(binName).exe $(cmdPackage)
build-win32:
	GOOS=windows GOARCH=386   go build $(LDFLAGS) -o bin/windows32/$(binName).exe $(cmdPackage)

build-all:
	@make build-mac
	@make build-linux64
	@make build-win64
	@make build-win32

clean:
	rm -rf bin

.PHONY: build dep clean

